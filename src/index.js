import "./styles/styles.css";
import {
  getCookies,
  createCookie,
  updateCookie,
  deleteCookie,
} from "./scripts/cookies.js";
import { createGUID } from "./scripts/helpers.js";

const addItemButton = document.getElementById("addItemBtn");
const clearItemsButton = document.getElementById("clearItemsBtn");
const list = document.getElementById("todoItemsList");
const inputField = document.getElementById("todoItemInput");
const errorMessage = document.getElementById("errorMsg");

const cookieMinutesToExpire = 5;
const emptyInputMessage = "Input should not be empty!";

clearItemsButton.addEventListener("click", clearItems);
addItemButton.addEventListener("click", addItem);

initialize();

function initialize() {
  let cookies = getCookies();
  for (let cookie of cookies) {
    let parts = cookie.split("="),
      id = parts[0],
      value = parts[1];
    //name structure: listItem:{uuid}
    let idParts = id.split(":");
    if (idParts[0].includes("listItem")) {
      createTodoItem(idParts[1], value);
    }
  }
}

function addItem() {
  updateErrorMsg("", "none");
  if (inputField.value == "") {
    updateErrorMsg(emptyInputMessage, "flex");
    return;
  }
  createTodoItem(createGUID(), inputField.value);
  inputField.value = "";

  function updateErrorMsg(message, display) {
    errorMessage.style.display = display;
    errorMessage.innerHTML = message;
  }
}

function createTodoItem(id, todoItemText) {
  let listItem = document.createElement("li");
  listItem.classList.add("list-item");
  listItem.id = id;

  let listItemContainer = document.createElement("div");
  listItemContainer.classList.add("list-item-container");
  listItemContainer.innerHTML = todoItemText;
  listItem.appendChild(listItemContainer);

  let editButton = createListItemButton("edit", editItem);
  listItem.appendChild(editButton);

  let deleteButton = createListItemButton("delete", deleteItem);
  listItem.appendChild(deleteButton);

  createCookie(id, todoItemText, cookieMinutesToExpire);
  list.appendChild(listItem);
  return listItem;
}

function createListItemButton(icon, action) {
  let button = document.createElement("button");
  button.classList.add("list-item-button");
  let buttonIcon = document.createElement("i");
  buttonIcon.classList.add("material-icons");
  buttonIcon.innerHTML = icon;
  button.appendChild(buttonIcon);
  button.addEventListener("click", action);
  return button;
}

function deleteItem(e) {
  let listElement = e.target.parentElement.parentElement;
  list.removeChild(listElement);
  deleteCookie(listElement.id);
}

function editItem(e) {
  let listItem = e.target.parentElement.parentElement;
  let listItemContainer = listItem.getElementsByClassName(
    "list-item-container"
  )[0];

  let newText = prompt(
    `Edit: ${listItemContainer.innerHTML}`,
    `${listItemContainer.innerHTML}`
  );
  if (newText !== null && newText !== "") {
    listItemContainer.innerHTML = newText;
    updateCookie(listItem.id, newText, cookieMinutesToExpire);
  }
}

function clearItems() {
  for (let element of list.children) {
    deleteCookie(element.id);
  }
  list.innerHTML = "";
}
