export function getCookies() {
  return document.cookie.split(";");
}

export function createCookie(id, value, minutesToExpire) {
  let expires = "";
  if (minutesToExpire) {
    let expire = new Date();
    expire.setMinutes(expire.getMinutes() + minutesToExpire);
    expires = `expires=${expire.toUTCString()};`;
  }
  document.cookie = `listItem:${id}=${value};${expires}`;
}

export function updateCookie(id, value, minutesToExpire) {
  createCookie(id, value, minutesToExpire);
}

export function deleteCookie(id) {
  createCookie(`${id}`, "", -1);
}
